package service.transaction;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import service.TestUtil;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@SpringBootTest
@AutoConfigureMockMvc
class MultipleTransactionsIntegrationTests {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void shouldReturnReportWithZeroBalance_afterSentListOfTransactions() throws Exception {
        //given
        String request = TestUtil.readResourceAsString("transaction/420_transactions_request.json");
        String expectedResponse = TestUtil.readResourceAsString("transaction/420_transactions_response.json");

        //when
        this.mockMvc.perform(post("/transactions/report")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andExpect(content().string(expectedResponse));
    }
}
