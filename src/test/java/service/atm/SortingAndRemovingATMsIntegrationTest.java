package service.atm;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import service.TestUtil;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

/**
 * Sorts and removes duplicated ATMs that has lower priority status.
 *
 * @author Dominik_Janiga
 */
@SpringBootTest
@AutoConfigureMockMvc
class SortingAndRemovingATMsIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void shouldReturnACollectionOfSortedATMs_whenSentListOf1000Regions() throws Exception {
        String atmsToSort = TestUtil.readResourceAsString("atm/duplicated_atms_request.json");
        String expectedResponse = TestUtil.readResourceAsString("atm/duplicated_atms_response.json");

        this.mockMvc.perform(post("/atms/calculateOrder")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(atmsToSort))
                .andExpect(MockMvcResultMatchers.content().string(expectedResponse));
    }
}
