package service.atm;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import service.TestUtil;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

/**
 * Sorting atm test.
 *
 * @author Dominik_Janiga
 */
@SpringBootTest
@AutoConfigureMockMvc
class ATMSortingIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void shouldReturnACollectionOfSortedATMs_whenSentListOfTasks() throws Exception {
        String atmsToSort = TestUtil.readResourceAsString("atm/tasks_request.json");
        String expectedResponse = TestUtil.readResourceAsString("atm/atms_response.json");

        this.mockMvc.perform(post("/atms/calculateOrder")
                .contentType(MediaType.APPLICATION_JSON)
                .content(atmsToSort))
                .andExpect(MockMvcResultMatchers.content().string(expectedResponse));
    }

    @Test
    void shouldReturnACollectionOfSortedATMs_whenSentListOf1000Regions() throws Exception {
        String atmsToSort = TestUtil.readResourceAsString("atm/1000_regions_request.json");
        String expectedResponse = TestUtil.readResourceAsString("atm/1000_regions_response.json");

        this.mockMvc.perform(post("/atms/calculateOrder")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(atmsToSort))
                .andExpect(MockMvcResultMatchers.content().string(expectedResponse));
    }
}
