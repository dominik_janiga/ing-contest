package service.game;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
class GameService {

    GroupsOrder calculateOrgerOfGroups(GameDto gameDto) {
        final Clans clans = gameDto.clans();
        final int groupCapacity = gameDto.groupCount();
        clans.sortByPointsDescending();
        List<Group> groups = new ArrayList<>();
        while (clans.hasClans()) {
            Group group = new Group(groupCapacity);
            Iterator<Clan> iterator = clans.iterator();
            while (iterator.hasNext()) {
                Clan clan = iterator.next();
                if (group.hasEnoughSpace(clan.numberOfPlayers())) {
                    group.addPlayers(clan);
                    iterator.remove();
                }
                if (group.isFull()) {
                    break;
                }
            }
            groups.add(group);
        }
        return new GroupsOrder(groups);
    }
}