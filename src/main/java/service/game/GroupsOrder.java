package service.game;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.List;

record GroupsOrder(@JsonValue List<Group> groups) {

}