package service.game;

record Clan(int numberOfPlayers, int points) implements Comparable<Clan> {

    private double calculatePointsPerPlayer() {
        return (double) this.points / this.numberOfPlayers;
    }

    @Override
    public int compareTo(Clan other) {
        int result = Integer.compare(this.points, other.points);
        if (result != 0) {
            return result;
        } else {
            double pointsPerPlayer = calculatePointsPerPlayer();
            double otherPointsPerPlayer = other.calculatePointsPerPlayer();
            return Double.compare(pointsPerPlayer, otherPointsPerPlayer);
        }
    }
}
