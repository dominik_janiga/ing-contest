package service.game;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/onlinegame")
@RestController
class GameController {

    private final GameService service;

    GameController(GameService service) {
        this.service = service;
    }

    @PostMapping("/calculate")
    GroupsOrder calculateOrgerOfGroups(@RequestBody GameDto gameDto) {
        return this.service.calculateOrgerOfGroups(gameDto);
    }
}