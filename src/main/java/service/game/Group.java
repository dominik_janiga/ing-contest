package service.game;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Dominik_Janiga
 */
class Group {

    private int capacity;
    @JsonValue
    private final List<Clan> clans;

    Group(int capacity) {
        this.capacity = capacity;
        this.clans = new ArrayList<>();
    }

    void addPlayers(Clan clan) {
        clans.add(clan);
        capacity -= clan.numberOfPlayers();
    }

    public boolean hasEnoughSpace(int numberOfPlayersToAdd) {
        return this.capacity >= numberOfPlayersToAdd;
    }

    public List<Clan> getClans() {
        return clans;
    }

    public boolean isFull() {
        return capacity == 0;
    }
}
