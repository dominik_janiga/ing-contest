package service.game;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

class Clans {

    private final List<Clan> clans;

    @JsonCreator
    Clans(List<Clan> clans) {
        this.clans = clans;
    }

    public Iterator<Clan> iterator() {
        return clans.iterator();
    }

    boolean hasClans() {
        return !clans.isEmpty();
    }

    public void sortByPointsDescending() {
        clans.sort(Comparator.reverseOrder());
    }

    public int size() {
        return clans.size();
    }
}