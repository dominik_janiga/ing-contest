package service.transaction;

import java.util.List;
import java.util.function.Supplier;

/**
 * @author Dominik_Janiga
 */
interface Storage<K, V> {

    V put(K key, V value);

    V putIfAbsent(K key, Supplier<V> value);

    List<V> getAccounts();
}
