package service.transaction;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

/**
 * @author Dominik_Janiga
 */
class AccountStorage<K, V extends Comparable<V>> implements Storage<K, V> {


    private final Map<K, V> storage;

    public AccountStorage(int capacity) {
        this.storage = new HashMap<>(capacity, 1f);
    }

    @Override
    public V put(K key, V value) {
        this.storage.put(key, value);
        return value;
    }

    @Override
    public V putIfAbsent(K key, Supplier<V> newValue) {
        V value = this.storage.get(key);
        if (value == null) {
            value = put(key, newValue.get());
        }
        return value;
    }

    @Override
    public List<V> getAccounts() {
        Collection<V> values = this.storage.values();
        return new ArrayList<>(values);
    }
}
