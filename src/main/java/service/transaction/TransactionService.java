package service.transaction;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author Dominik_Janiga
 */
@Service
final class TransactionService {

    private Storage<AccountNumber, Account> storage;

    Accounts getTransactionReport(Transactions transactions) {
        int numberOfAccounts = transactions.size() * 2;
        this.storage = new AccountStorage<>(numberOfAccounts);
        for (Transaction transaction : transactions) {
            handleCreditOperation(transaction);
            handleDebitOperation(transaction);
        }

        List<AccountDto> accounts = storage.getAccounts().stream().sorted().map(Account::toDto).toList();
        return new Accounts(accounts);
    }

    private void handleDebitOperation(Transaction transaction) {
        BigDecimal amount = transaction.getAmount();
        AccountNumber debitAccount = transaction.getDebitAccount();
        Account account = putAccountIfAbsent(debitAccount);
        account.performDebitTransaction(amount);
    }

    private void handleCreditOperation(Transaction transaction) {
        BigDecimal amount = transaction.getAmount();
        AccountNumber creditAccount = transaction.getCreditAccount();
        Account account = putAccountIfAbsent(creditAccount);
        account.performCreditTransaction(amount);
    }

    private Account putAccountIfAbsent(AccountNumber accountNumber) {
        return this.storage.putIfAbsent(accountNumber, () -> new Account(accountNumber));
    }
}
