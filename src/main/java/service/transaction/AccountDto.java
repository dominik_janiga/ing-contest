package service.transaction;

import java.math.BigDecimal;

/**
 * @author Dominik_Janiga
 */
record AccountDto(AccountNumber account, int debitCount, int creditCount, BigDecimal balance) {

}

