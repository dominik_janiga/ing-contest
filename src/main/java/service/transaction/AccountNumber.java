package service.transaction;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Objects;

/**
 * @author Dominik_Janiga
 */
final class AccountNumber implements Comparable<AccountNumber> {

    @JsonValue
    private final String account;

    AccountNumber(String account) {
        this.account = account;
    }

    public String getAccount() {
        return account;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountNumber that = (AccountNumber) o;
        return Objects.equals(account, that.account);
    }

    @Override
    public int hashCode() {
        return this.account.hashCode();
    }

    @Override
    public int compareTo(AccountNumber accountNumber) {
        return this.account.compareTo(accountNumber.account);
    }
}
