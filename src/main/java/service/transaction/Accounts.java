package service.transaction;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.List;

/**
 * @author Dominik_Janiga
 */
record Accounts(@JsonValue List<AccountDto> account) {
}
