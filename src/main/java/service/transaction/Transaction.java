package service.transaction;

import java.math.BigDecimal;

/**
 * @author Dominik_Janiga
 */
final class Transaction {

    private final AccountNumber debitAccount;
    private final AccountNumber creditAccount;
    private final BigDecimal amount;

    Transaction(AccountNumber debitAccount,
                AccountNumber creditAccount,
                BigDecimal amount) {
        this.debitAccount = debitAccount;
        this.creditAccount = creditAccount;
        this.amount = amount;
    }

    public AccountNumber getDebitAccount() {
        return debitAccount;
    }

    public AccountNumber getCreditAccount() {
        return creditAccount;
    }

    public BigDecimal getAmount() {
        return amount;
    }
}
