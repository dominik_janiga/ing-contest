package service.transaction;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.Iterator;
import java.util.List;
import java.util.Spliterator;
import java.util.function.Consumer;

/**
 * @author Dominik_Janiga
 */
class Transactions implements Iterable<Transaction> {

    private final List<Transaction> transactions;

    @JsonCreator
    public Transactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    @Override
    public void forEach(Consumer<? super Transaction> action) {
        this.transactions.forEach(action);
    }

    @Override
    public Spliterator<Transaction> spliterator() {
        return this.transactions.spliterator();
    }

    @Override
    public Iterator<Transaction> iterator() {
        return this.transactions.iterator();
    }

    int size() {
        return transactions.size();
    }
}
