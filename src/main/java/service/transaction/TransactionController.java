package service.transaction;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Dominik_Janiga
 */
@RestController
@RequestMapping("/transactions")
final class TransactionController {

    private final TransactionService transactionService;

    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @PostMapping("/report")
    Accounts getTransactionReport(@RequestBody Transactions transactions) {
        return this.transactionService.getTransactionReport(transactions);
    }
}
