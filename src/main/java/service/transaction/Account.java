package service.transaction;

import java.math.BigDecimal;

/**
 * @author Dominik_Janiga
 */
class Account implements Comparable<Account> {

    private final AccountNumber account;
    private int debitCount;
    private int creditCount;
    private BigDecimal balance;

    Account(AccountNumber account) {
        this.account = account;
        this.balance = new BigDecimal(0);
    }

    public AccountNumber getAccount() {
        return account;
    }

    public int getDebitCount() {
        return debitCount;
    }

    public int getCreditCount() {
        return creditCount;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    private void incrementDebitCount() {
        this.debitCount++;
    }

    private void incrementCreditCount() {
        this.creditCount++;
    }

    private void increaseBalance(BigDecimal amount) {
        this.balance = this.balance.add(amount);
    }

    private void decreaseBalance(BigDecimal amount) {
        this.balance = this.balance.subtract(amount);
    }

    void performCreditTransaction(BigDecimal amount) {
        increaseBalance(amount);
        incrementCreditCount();
    }

    void performDebitTransaction(BigDecimal amount) {
        decreaseBalance(amount);
        incrementDebitCount();

    }

    AccountDto toDto() {
        return new AccountDto(this.account, this.debitCount, this.creditCount, this.balance);
    }

    @Override
    public int compareTo(Account account) {
        return this.account.compareTo(account.account);
    }
}
